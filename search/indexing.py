from math import *
import time
import search.corpus

class Index():
    def __init__(self, word=""):
        self.word = word
        self.postings = {}
        self.genre_counts = {}

    def emplace_posting(self, doc_id=0, freq=0):
        self.postings[doc_id] = freq
   
    def remove_posting(self, doc_id):
        del self.postings[doc_id]

    def add_genre(self, genre):
        if not genre in self.genre_counts:
            self.genre_counts[genre] = 0
        
        self.genre_counts[genre] += 1
     
    def get_posting(self, doc_id):
        if doc_id in self.postings:
            return self.postings[doc_id]
        else:
            return 0

class Result():

    def __init__(self, doc_id, cos):
        self.doc_id = doc_id
        self.cos = cos

    def __lt__(self, other):
         return self.cos < other.cos

class InvertedIndex():
    def __init__(self, documents):
        self.__index = {}
        self.__document_terms = {}
        self.__document_freq = {}
        self.__idf = {}
        self.evidence = {}
        self.genre_count = {}
        self.documents = documents

    def size(self):
        return len(self.__index)

    def compute_idf(self):
        for index_key in self.__index:    
            self.__idf[index_key] = len(self.__index) / float(self.__document_freq[index_key])

    def query_term_count(self, query_vec, term):
        count = 0
        for x in query_vec:
            if x == term:
                count += 1

        return count
    
    def query_tf(self, query_vec, term):
        return float(self.query_term_count(query_vec, term)) / len(query_vec)

    def compute_mag(self, vec):
        sum = 0.0
        for x in vec:
            sum += float(x) * float(x)
        return sqrt(sum)

    def make_idf(self, vec):
        vec_m = []

        # Generate a visual representation of the IDF calculations
        for x in vec:
            vec_m.append("ln(%d / %d) = <strong>%.5f</strong>" % (len(self.__index), self.df_unnormalized(x), self.idf(x)) )
        
        return vec_m

    def make_tf_query(self, query):
        vec_m = []

        for x in query:
            vec_m.append("%d / %d = <strong>%.5f</strong>" % (self.query_term_count(query, x), len(query), self.query_tf(query, x)))
        
        return vec_m

    
    def make_tf_doc(self, query, doc):
        vec = []
        doc_id = doc.doc_id

        for word in query:
            vec.append("%d / %d = <strong>%.5f</strong>" % (self.get_index_freq(word, doc_id), self.__document_terms[doc_id], self.tf(word, doc_id)))

        return vec
    

    def make_dot(self, query, doc):
        dot = ""
        doc_id = doc.doc_id
        query_vec = self.make_tfidf(query)

        # Compute document vector
        for word in query:
            tf = self.tf(word, doc_id)
            idf = self.idf(word)
            doc_vec.append(tf*idf)
    

        for word in query:
            vec.append("%.4f * %.4f = <strong>%.4f</strong>" % (self.tf(word, doc_id), self.idf(word), self.tf(word, doc_id) * self.idf(word)))

        return vec

    def make_tfidf(self, vec):
        vec_m = []
        for x in vec:
            vec_m.append(self.query_tf(vec, x) * self.idf(x))
        
        return vec_m

    def make_tfidf_docu(self, vec, doc_id):
        vec_m = []
        for x in vec:
            vec_m.append(self.tf(x, doc_id) * self.idf(x))
        
        return vec_m

    def compute_cos(self, vec1, vec2):
        dot = self.compute_dot(vec1, vec2)
        mag1 = self.compute_mag(vec1)
        mag2 = self.compute_mag(vec2)
        
        return dot / (mag1 * mag2)
       
    def compute_dot(self, vec1, vec2):
        dot = 0.0
        
        for i in range(len(vec1)):
            dot += vec1[i] * vec2[i]

        return dot

    def make_tfidf_doc_calc(self, query, doc):
        vec = []
        doc_id = doc.doc_id

        for word in query:
            vec.append("%.4f * %.4f = <strong>%.4f</strong>" % (self.tf(word, doc_id), self.idf(word), self.tf(word, doc_id) * self.idf(word)))

        return vec

    def make_doc_dot_calc(self, query, doc):
        dot_calc = ""
        doc_id = doc.doc_id
        query_vec = self.make_tfidf(query)
        doc_vec = self.make_tfidf_docu(query, doc_id)
        dot = self.compute_dot(query_vec, doc_vec)

        for i in range(len(query)):
            dot_calc += "%.4f * %.4f" % (query_vec[i], doc_vec[i])

            if i < len(query) - 1:
                dot_calc += " + " 

        dot_calc += " = <strong>%.5f</strong>" % dot    

        return dot_calc

    def make_cos_sim_calc(self, query, doc):
        doc_id = doc.doc_id
        query_vec = self.make_tfidf(query)
        doc_vec = self.make_tfidf_docu(query, doc_id)
        dot = self.compute_dot(query_vec, doc_vec)
        query_vec_mag = self.compute_mag(query_vec)
        doc_vec_mag = self.compute_mag(doc_vec)
        cos_sim = self.compute_cos(query_vec, doc_vec)
        return "(%.4f) / (%.4f * %.4f) = <strong>%.4f</strong>" % (dot, query_vec_mag, doc_vec_mag, cos_sim)

    def phrase_search(self, cluster, query, verbatim):  
        docs = cluster.get_docs()
        phrase_corpus = search.corpus.Corpus()

        for doc_id in docs:
            should_include = True

            for x in query:
                if self.get_index_freq(x, doc_id) == 0:
                    should_include = False

            document = cluster.get_doc(doc_id)

            if should_include and ("%s " % verbatim) in document.contents:
                phrase_corpus.add_document_norm(document)

        return self.search(phrase_corpus, query)

    def search(self, cluster, query):
        docs = cluster.get_docs()
        query_vec = self.make_tfidf(query)
        results = []
        
        for doc_id in docs:

            doc_vec = self.make_tfidf_docu(query, doc_id)
            doc_vec_mag = self.compute_mag(doc_vec)

            if(doc_vec_mag > 0.0):
                #print("%f %f %f %f" % (cos_sim, dot, query_vec_mag, doc_vec_mag))
                cos_sim = self.compute_cos(query_vec, doc_vec)
                result = Result(doc_id, cos_sim)
                results.append(result)

        sorted_results = sorted(results)
        
        return sorted_results

    def df_unnormalized(self, term):
        return self.df(term) * len(self.__index)
    
    def df(self, term):
        if term in self.__idf:
            return 1.0 / self.__idf[term]
        else:
            return 0

    def idf(self, term):
        if term in self.__idf:
            return log(float(self.__idf[term]))
        else:
            return 1000000000
    
    def posterior_vec(self, vector, corpus):
        result = {}

        evidence = 0.0
#        for feature in vector:
#            evidence += self.feature_prob(feature)

        for genre in self.genre_count:
            
            # P(C)
            product = self.class_prob(genre)
          
            for feature in vector:
                # P(x | C)
                product *= (self.feature_likelihood(feature, genre))
                print(("%s, %s: %.3f %.3f %.3f") % (genre, feature, self.feature_likelihood(feature, genre), self.class_prob(genre), evidence))

            evidence += product

            # / P(x)
            result[genre] = product

        if evidence > 0.0:
            for r in result:
                result[r] /= evidence

        return result

    def posterior(self, genre, feature):

        if not feature in self.__index:
            return 0.0


        if not genre in self.__index[feature].genre_counts:
            return 0.0
        else:
            return (self.feature_likelihood(feature, genre) * self.class_prob(genre)) / (self.feature_prob(feature) + 0.00000001)

    def class_prob(self, genre):
        return self.genre_count[genre] / self.documents
    
    def feature_likelihood(self, feature, genre):
        
        if feature in self.__index:
            if genre in self.__index[feature].genre_counts:
                return self.__index[feature].genre_counts[genre] / self.genre_count[genre]
            else:
                return 0.0
        else:
            return 0.0

    def feature_prob(self, feature):
        
        if not feature in self.evidence:
            return 0.0
        else:
            return self.evidence[feature] / self.documents

    def post_ev(self, index):
        if not index in self.evidence:
            self.evidence[index] = 0
        
        self.evidence[index] += 1

    def post_genre(self, index, genre):
        self.__index[index].add_genre(genre)

    def post_index(self, index, doc_id):
        new_freq = self.get_index_freq(index, doc_id) + 1
        self.emplace_index(index, doc_id, new_freq)

        if doc_id in self.__document_terms:
            self.__document_terms[doc_id] += 1
        else:
            self.__document_terms[doc_id] = 1

        if index in self.__document_freq:
            self.__document_freq[index] += 1
        else:
            self.__document_freq[index] = 1

    def get_indices(self):
        return self.__index.keys()

    def tf(self, index, doc_id):
        return float(self.get_index_freq(index, doc_id)) / float(self.__document_terms[doc_id])

    def emplace_index(self, ind, doc_id, freq):
        if not ind in self.__index:
            self.__index[ind] = Index(ind)
        self.__index[ind].emplace_posting(doc_id, freq)
    
    def remove_index(self, index):
        del self.__index[index]
    
    def get_index(self, ind):
        return self.__index[ind]
    
    def get_index_freq(self, ind, doc_id):
        if not ind in self.__index:
            return 0
        else:    
            return self.__index[ind].get_posting(doc_id)

def display_index(index):
    print("Displaying index\n")
    for index_key in index.get_indices():
        print('%s: ' % (index_key), end='')
        index_entry = index.get_index(index_key)
        for doc_id in index_entry.postings.keys():
            posting = index_entry.get_posting(doc_id)
            print("(%d, %d, %f, %f), " % (doc_id, posting, index.tf(index_key, doc_id), index.idf(index_key)), end='')
        print('')

def display_index_idf(index):
    print("Displaying index\n")
    for index_key in index.get_indices():
        print('%s: %f' % (index_key, index.idf(index_key)))