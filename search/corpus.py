from search.indexing import *
from search.transform import *
import json

class Document():
    def __init__(self, doc_id, name, img_url, contents="", genres=[]):
        self.doc_id = doc_id
        self.contents = contents
        self.vector = set()
        self.name = name
        self.img_url = img_url
        self.genres = genres
    
    def gen_html(self, vec, is_phrase, verbatim):
        built_text = ""

        if(is_phrase):
            words = self.contents.split(verbatim)
            built_text = words[0] + "<strong>" + verbatim + "</strong>" + words[1]
        else:
            words = self.contents.split(" ")
            for i in range(len(words) - 1):
                current_word = words[i]
                transformed = transform_word(current_word)
                if transformed in vec:
                    built_text += "<strong>" + current_word + " </strong>"
                else:
                    built_text += current_word + " "

        return built_text

class Corpus():
    def __init__(self):
        self.docs = {}
        self.next_doc = 0

    def add_document(self, name, img_url, contents, genres):
        self.docs[self.next_doc] = Document(self.next_doc, name, img_url, contents, genres)
        self.next_doc += 1
    
    def add_document_norm(self, document):
        self.docs[document.doc_id] = document

    def remove_document(self, doc_id):
        del self.docs[doc_id]
    
    def get_doc(self, doc_id):
        return self.docs[doc_id]

    def get_docs(self):
        return self.docs.keys()

def load_coco_corpus(file):
    images = Corpus()

    with open(file) as f:
        images_json = json.load(f)

        for annotation in images_json['annotations']:
            image_id = annotation['image_id']
            caption = annotation['caption']
            images.add_document("", ("http://images.cocodataset.org/val2014/COCO_val2014_%012d.jpg") % (image_id), caption, [])

    return images

def load_games_cluster(file):
    cluster = Corpus()
    
    with open(file, encoding='utf8') as f:
        for line in f:
            columns = line.split(",")
            name = columns[2]
            img_url = columns[67]
            genres = []

            is_coop = columns[37]
            is_mmo  = columns[38]
            is_vr  = columns[42]
            is_nongame  = columns[43]
            is_indie  = columns[44]
            is_action  = columns[45]
            is_adventure  = columns[46]
            is_casual  = columns[47]
            is_strategy  = columns[48]
            is_rpg  = columns[49]
            is_sim  = columns[50]
            #is_early  = bool(columns[38])
            is_sports  = columns[53]
            is_racing  = columns[54]

            if is_coop == "True":
                genres.append("coop")
            if is_mmo == "True":
                genres.append("mmo")
            if is_vr == "True":
                genres.append("vr")
            if is_action == "True":
                genres.append("action")
            if is_adventure == "True":
                genres.append("adventure")
            if is_casual == "True":
                genres.append("casual")
            if is_strategy == "True":
                genres.append("strategy")
            if is_rpg == "True":
                genres.append("rpg")
            if is_sim == "True":
                genres.append("sim")
            if is_sports == "True":
                genres.append("sports")
            if is_racing == "True":
                genres.append("racing")

            # Throw out trash row.
            if(name == "QueryName"):
                continue

            description = columns[64]
            cluster.add_document(name, img_url, description, genres)
    
    return cluster
  
def preprocess_cluster(cluster):
    # This is where the cluster will be converted into an inverted index
    
    inverted_index = InvertedIndex(len(cluster.docs))
    documents = 0
    for doc_id in cluster.docs:
        document = cluster.get_doc(doc_id)
        contents = document.contents
        tokens = contents.split(' ')

        visited_words = []

        for word in tokens:
            
            if is_relevant(word):
                index = transform_word(word)
                inverted_index.post_index(index, doc_id)
                document.vector.add(index)

            visited_words.append(word)
        
        documents += 1

        if documents % 100 == 0:
            print("%d documents processed" % (documents))

    print("Index size: %d" % (inverted_index.size()))

    return inverted_index

def train_model(cluster, inverted_index):
    # This is where the cluster will be converted into an inverted index
    
    documents = 0
    for doc_id in cluster.docs:
        document = cluster.get_doc(doc_id)

        # Add the genres to the running total of each genre
        for genre in document.genres:
            if not genre in inverted_index.genre_count:
                inverted_index.genre_count[genre] = 0
            inverted_index.genre_count[genre] += 1

        for index in document.vector:
            # Add the dependent class probabilities of each feature
            for genre in document.genres:
                inverted_index.post_genre(index, genre)

            # Add to the evidence of this particular feature
            inverted_index.post_ev(index)
        
        documents += 1

        if documents % 100 == 0:
            print("%d documents trained" % (documents))

    return inverted_index