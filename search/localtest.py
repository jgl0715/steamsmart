from cluster import *
import transform
import math

print('Loading dataset...')
cluster = load_cluster('data/steam_games.csv')

print('Preprocessing cluster...')
index = preprocess_cluster(cluster)

print('Computing idf...')
index.compute_idf()

search_query = input('input query: ')
while search_query != "quit":
    query = make_search_query(search_query)
    print(search_query)
    print(query)

    ranked = index.search(cluster, query)

    first = len(ranked)
    last = max(0, len(ranked) - 100)
    for i in range(0, min(100, len(ranked))):
        actual = first - i - 1
        doc_id = ranked[actual].doc_id
        print("%d: %s (cos=%f)" % (i + 1, cluster.get_doc(doc_id).name, ranked[actual].cos))
    
    search_query = input('input query: ')



