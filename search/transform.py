import nltk
import nltk.stem
import nltk.stem.porter
from nltk.corpus import stopwords
 
# This determines which words actually become indices in the inverted index

stemmer = nltk.stem.porter.PorterStemmer()
stopwords = set(stopwords.words('english'))

def make_search_query(text):

    query_vec = []
    
    for x in text.split(' '):
        if is_relevant(x):
            query_vec.append(transform_word(x))
    
    return query_vec

def is_relevant(word):
    # This is where stop word removal would occur
    return not word in stopwords

def transform_word(word):
    # This is where stemming and lemmatization would occur
    return stemmer.stem(word)