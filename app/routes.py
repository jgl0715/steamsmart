from app.forms import *
from app import flask_app
from app import built_index
from app import cluster
from app.view import HtmlResult
from flask import render_template, flash, redirect
from transform import *

@flask_app.route('/', methods=['GET', 'POST'])
@flask_app.route('/index', methods=['GET', 'POST'])
def index():
    form = SearchForm()
    html_results = None 

    if form.validate_on_submit():
        flash('working')

        ranked = built_index.search(cluster, make_search_query(form.search.data))

        # Build html view results
        html_results = []

        first = len(ranked)
        for i in range(0, min(100, len(ranked))):
            actual = first - i - 1
            doc_id = ranked[actual].doc_id
            doc = cluster.get_doc(doc_id)

            html_result = HtmlResult(doc.name, doc.img_url)
            html_results.append(html_result)

    return render_template('index.html', form=form, results=html_results)

