
class QueryColumn():

    def __init__(self, term, val, idf, tf):
        self.term = term
        self.value = val
        self.idf = idf
        self.tf = tf

class ClassifyResult:

    def __init__(self, genre, probability=0.0, calc=""):
        self.genre = genre
        self.probability = probability
        self.calculation = calc

    def __lt__(self, other):
        return self.probability < other.probability

class Meta():
    def __init__(self, query, query_tf, query_idf, query_tfidf, delta, results_count):
        self.query_columns = []
        self.delta = delta
        self.results = results_count

        # Build the visual representation of the query vector
        for i in range(0, len(query)):
            self.query_columns.append( QueryColumn(query[i], ("<strong>%.5f</strong>" % (query_tfidf[i])), query_idf[i], query_tf[i]) )

class HtmlResult():

    def __init__(self, name, img_url):
        self.name = name
        self.img_url = img_url
        self.bg_color = ""
        self.desc = ""
        self.tf = []
        self.idf = []
        self.tfidf = []
        self.dot = ""
        self.cos_sim = ""