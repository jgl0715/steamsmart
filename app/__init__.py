import math
from app.view import HtmlResult, Meta, ClassifyResult
from app.forms import *
from search.corpus import *
from search.transform import *
from classify.model import *
from flask import Flask, render_template, flash, redirect

print('Loading games dataset...')
games_corpus = load_games_cluster('data/steam_games.csv')

print('Loading and training caption dataset...')
caption_corpus = load_coco_corpus('data/captions_val2014.json')

print('Preprocessing games corpus...')
built_games_index = preprocess_cluster(games_corpus)

print('Preprocessing caption corpus...')
built_caption_index = preprocess_cluster(caption_corpus)

print('Computing idf...')
built_games_index.compute_idf()
built_caption_index.compute_idf()

print('Training naive bayes model...')
train_model(games_corpus, built_games_index)

flask_app = Flask(__name__)

@flask_app.route('/', methods=['GET', 'POST'])
@flask_app.route('/index', methods=['GET', 'POST'])
def index():

    form = SearchForm()
    html_results = None
    meta = None

    if form.validate_on_submit():

        is_phrase_search = False
        if len(form.search.data) > 0 and (form.search.data[0] == '"') and (form.search.data[len(form.search.data) - 1] == '"'):
            is_phrase_search = True

        search_query = None
        start_time = int(round(time.time() * 1000))

        ranked = None
        verbatim = None
        if is_phrase_search:
            verbatim = form.search.data[1 : len(form.search.data) - 1]
            search_query = make_search_query(verbatim)
            ranked = built_games_index.phrase_search(games_corpus, search_query, verbatim)
        else:
            search_query = make_search_query(form.search.data)
            ranked = built_games_index.search(games_corpus, search_query)
            
        end_time = int(round(time.time() * 1000))
        delta = float((end_time - start_time) / 1000.0)

        search_tfidf = built_games_index.make_tfidf(search_query)
        search_tf = built_games_index.make_tf_query(search_query)
        search_idf = built_games_index.make_idf(search_query)


        # Build html view results
        html_results = []

        rc = len(ranked)
        last = min(20, len(ranked))

        for i in range(0, last):


            actual = rc - i - 1
            doc_id = ranked[actual].doc_id
            doc = games_corpus.get_doc(doc_id)
            doc_tf = built_games_index.make_tf_doc(search_query, doc)
            doc_tfidf = built_games_index.make_tfidf_doc_calc(search_query, doc)
            doc_dot = built_games_index.make_doc_dot_calc(search_query, doc)
            doc_cos_sim = built_games_index.make_cos_sim_calc(search_query, doc)

            # Determines the background color based on the linear interpolant from red to blue
            t = float(i) / last
            red = hex(int((1 - t) * 255))[2:].zfill(2)
            green = "00"
            blue = hex(int(t * 255))[2:].zfill(2)

            # Build the visual result for this rank entry 
            html_result = HtmlResult(str(i+1) + ". " + doc.name, doc.img_url)
            html_result.bg_color = "#" + red + green + blue
            html_result.desc = doc.gen_html(search_query, is_phrase_search, verbatim)
            html_result.tf = doc_tf
            html_result.idf = search_idf
            html_result.tfidf = doc_tfidf
            html_result.dot = doc_dot
            html_result.cos_sim = doc_cos_sim
            html_results.append(html_result)

        meta = Meta(search_query, search_tf, search_idf, search_tfidf, delta, rc)

    return render_template('index.html', form=form, results=html_results, meta=meta)

@flask_app.route('/classify', methods=['GET', 'POST'])
def classify():
    
    form = SearchForm()
    results = []

    if form.validate_on_submit():
        feature_vec = make_search_query(form.search.data)
        class_post = built_games_index.posterior_vec(feature_vec, caption_corpus)

        for genre in class_post:
            result = ClassifyResult(genre, class_post[genre], ("p(%s)=%.3f") % (genre, class_post[genre]))
            results.append(result)

        results = sorted(results)
        results.reverse()

    return render_template('classify.html', form=form, results=results)

@flask_app.route('/caption', methods=['GET', 'POST'])
def caption():

    form = SearchForm()
    html_results = None
    meta = None

    if form.validate_on_submit():

        is_phrase_search = False
        if len(form.search.data) > 0 and (form.search.data[0] == '"') and (form.search.data[len(form.search.data) - 1] == '"'):
            is_phrase_search = True

        search_query = None
        start_time = int(round(time.time() * 1000))

        ranked = None
        verbatim = None
        if is_phrase_search:
            verbatim = form.search.data[1 : len(form.search.data) - 1]
            search_query = make_search_query(verbatim)
            ranked = built_caption_index.phrase_search(caption_corpus, search_query, verbatim)
        else:
            search_query = make_search_query(form.search.data)
            ranked = built_caption_index.search(caption_corpus, search_query)
            
        end_time = int(round(time.time() * 1000))
        delta = float((end_time - start_time) / 1000.0)

        search_tfidf = built_caption_index.make_tfidf(search_query)
        search_tf = built_caption_index.make_tf_query(search_query)
        search_idf = built_caption_index.make_idf(search_query)


        # Build html view results
        html_results = []

        rc = len(ranked)
        last = min(20, len(ranked))

        for i in range(0, last):


            actual = rc - i - 1
            doc_id = ranked[actual].doc_id
            doc = caption_corpus.get_doc(doc_id)
            doc_tf = built_caption_index.make_tf_doc(search_query, doc)
            doc_tfidf = built_caption_index.make_tfidf_doc_calc(search_query, doc)
            doc_dot = built_caption_index.make_doc_dot_calc(search_query, doc)
            doc_cos_sim = built_caption_index.make_cos_sim_calc(search_query, doc)

            # Determines the background color based on the linear interpolant from red to blue
            t = float(i) / last
            red = hex(int((1 - t) * 255))[2:].zfill(2)
            green = "00"
            blue = hex(int(t * 255))[2:].zfill(2)

            # Build the visual result for this rank entry 
            html_result = HtmlResult(str(i+1) + ". " + doc.name, doc.img_url)
            html_result.bg_color = "#" + red + green + blue
            html_result.desc = doc.gen_html(search_query, is_phrase_search, verbatim)
            html_result.tf = doc_tf
            html_result.idf = search_idf
            html_result.tfidf = doc_tfidf
            html_result.dot = doc_dot
            html_result.cos_sim = doc_cos_sim
            html_results.append(html_result)

        meta = Meta(search_query, search_tf, search_idf, search_tfidf, delta, rc)

    return render_template('caption.html', form=form, results=html_results, meta=meta)


print('Starting website...')
flask_app.config['SECRET_KEY'] = 'you will never guess'
flask_app.config['TEMPLATE_AUTO_RELOAD'] = True
flask_app.run(debug = True, host='0.0.0.0')
