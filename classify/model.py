from search.indexing import * 
from search.transform import *

class Model:

    def __init__(self, index):
        self.__class_counts = {}
        self.__document_count = 0
        self.__feature_table = {}
        self.__index = index

    def posterior(self, in_class, feature):
        return (self.feature_likelihood(feature, in_class) * self.class_prob(in_class)) / self.feature_prob(feature)

    def class_prob(self, in_class):
        return self.__class_counts / self.__document_count
    
    def feature_likelihood(self, feature, in_class):
        return self.__feature_table[in_class] / self.__class_counts[in_class]

    def feature_prob(self, feature):
        return self.__index.df(feature)
    
    def post_class(self, in_class):

        if not in_class in self.__class_counts:
            self.__class_counts[in_class] = 0

        self.__class_counts[in_class] += 1
    
    def post_feature(self, feature, in_class):
        if not feature in self.__feature_table:
            self.__feature_table[feature] = {}

        if not in_class in self.__feature_table[feature]:
            self.__feature_table[feature] = 0

        self.__feature_table[feature] += 1

def load_trained_model(corpus, index):
    result = Model(index)
    result.document_count = len(corpus.docs)

    for doc_id in corpus.get_docs():
        document = corpus.get_doc(doc_id)

        for genre in document.genres:
            result.post_class(genre)
        
    return result

